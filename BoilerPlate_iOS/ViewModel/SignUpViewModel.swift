//
//  SignUpViewModel.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 22/09/21.

import Foundation
import Alamofire
import Toast_Swift

class SignUpViewModel {
    
    var emailStr: String?
    var fname: String?
    var lname: String?
    var password : String?
    var confirmpass : String?
    var style = ToastStyle()
    var delegate : LoginSignUpDelegate?

    
    
    func checkValidFields() {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let topVC = GlobalFunctions.topViewController(in: keyWindow?.rootViewController)!
        self.style.backgroundColor = .red
        
        guard fname != nil, !fname!.isEmpty  else {
            return topVC.view.makeToast(UserErrorMessages.emptyFirstNameMsg,  duration: 3.0, position: .bottom, style: self.style)
            
        }
        guard lname != nil, !lname!.isEmpty else {
            return topVC.view.makeToast(UserErrorMessages.emptylastNameMsg,  duration: 3.0, position: .bottom, style: self.style)
            
        }
        guard emailStr != nil, !emailStr!.isEmpty else {
            return topVC.view.makeToast(UserErrorMessages.emptyEmailMsg,  duration: 3.0, position: .bottom, style: self.style)
            
        }
        if !Validation().isValidEmail(testStr: emailStr!) {
            return topVC.view.makeToast(UserErrorMessages.invalidEmailMsg,  duration: 3.0, position: .bottom, style: self.style)
            
        }
        guard password != nil, !password!.isEmpty else {
            return topVC.view.makeToast(UserErrorMessages.emptyPasswordMsg,  duration: 3.0, position: .bottom, style: self.style)
            
        }
        if !Validation().validatePassword(testStr: password!) {
            return topVC.view.makeToast(UserErrorMessages.invalidPasswordMsg,  duration: 3.0, position: .bottom, style: self.style)
            
        }
        guard confirmpass != nil, !confirmpass!.isEmpty else {
            return topVC.view.makeToast(UserErrorMessages.emptyConfirmPasswordMsg,  duration: 3.0, position: .bottom, style: self.style)
            
        }
        if password != confirmpass {
            return topVC.view.makeToast(UserErrorMessages.confirmPasswordMismatch,  duration: 3.0, position: .bottom, style: self.style)
        }
        signUpApiRequest()
    }
    
    func signUpApiRequest() {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let topVC = GlobalFunctions.topViewController(in: keyWindow?.rootViewController)!
        let userEmail = (emailStr == nil) ? nil : (self.emailStr!.lowercased()).trimmingCharacters(in: .whitespaces)
        let userNameModel = FullName(first: fname, last: lname)
        let userReqModel = User(email: userEmail, password: password, name: userNameModel)
        let registerReq = LoginRequestModel(user: userReqModel)
        let dict = registerReq.dictionary ?? [:]
        GlobalFunctions.printToConsole(message: "signup param:- \(dict)")
        AuthenticationServices.userSignUp(userDict: dict) { (signUpResponse, errorStr) in
            
            // Handle api response
            guard signUpResponse != nil else {
                self.style.backgroundColor = .red
                topVC.view.makeToast(errorStr,  duration: 3.0, position: .bottom, style: self.style)
                
                return
            }
            self.style.backgroundColor = .green
            topVC.view.makeToast(UserErrorMessages.signUpSuccessMsg,  duration: 3.0, position: .bottom, style: self.style)
            self.delegate?.completedProcess(isLogin: false, sMsg: UserErrorMessages.loginSuccessMsg, isError: false)

        }
    }
    
}
