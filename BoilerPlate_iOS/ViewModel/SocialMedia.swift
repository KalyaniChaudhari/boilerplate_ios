////
////  SocialLoginViewModel.swift
////  BoilerPlate_iOS
////
////  Created by Kalyani on 23/09/21.
////
//
import Foundation
import FBSDKLoginKit
import SVProgressHUD


////#MARK:- Facebook login
class FacebookSignUp {
    
    static func facebookLogIn(currentVC: UIViewController, completionHandler: @escaping(SocialLoginRequestModel) -> ()) {
        let loginManager = LoginManager()
        if let accessToken = AccessToken.current {
            // Access token available -- user already logged in
            // Perform log out
            loginManager.logOut()
            //            Call again this method to login the user
            if let login = currentVC as? LoginVC {
                FacebookSignUp.facebookLogIn(currentVC: login) { (userObj) in
                    login.callSocialLoginSignUpApi(loginObj: userObj)
                }
            } else {
                if let signup = currentVC as? SignUpVC {
                    FacebookSignUp.facebookLogIn(currentVC: signup) { (userObj) in
                       // signup.callSocialLoginSignUpApi(loginObj: userObj)
                    }
                }
            }
            
        } else {
            // Access token not available -- user already logged out
            // Perform log in
            loginManager.logIn(permissions: ["public_profile", "email"], from: currentVC) { (loginResult, error) in
                // Check for error
                guard error == nil else {
                    // Error occurred
                    GlobalFunctions.printToConsole(message: error!.localizedDescription)
                    return
                }
                
                // Check for cancel
                guard let result = loginResult, !result.isCancelled else {
                    GlobalFunctions.printToConsole(message: "User cancelled login")
                    return
                }
                // Successfully logged in
                Profile.loadCurrentProfile { (profile, error) in
                    if let userProfile = profile {
                        GlobalFunctions.printToConsole(message: "Facebook login token:- \(AccessToken.current?.tokenString)")
                        GlobalFunctions.printToConsole(message: "User last email:- \(userProfile.email)")
                        GlobalFunctions.printToConsole(message: "User last name:- \(userProfile.name)")
                        GlobalFunctions.printToConsole(message: "User profile:- \(userProfile.imageURL(forMode: .square, size: CGSize(width: 200, height: 200)))")
                        GlobalFunctions.printToConsole(message: "User birthdate:- \(userProfile.birthday)")
                        GlobalFunctions.printToConsole(message: "User gender:- \(userProfile.gender)")
                       // let profileImgUrl = userProfile.imageURL(forMode: .square, size: CGSize(width: 200, height: 200))
                        let fname = FullName(first: userProfile.firstName, last: userProfile.lastName)
                        let obj = SocialLoginRequestModel(name:fname , oauth: "FACEBOOK", email:  userProfile.email!)
                      //  let obj = SocialLoginRequestModel(name: userProfile.name, email: userProfile.email, profile: profileImgUrl?.absoluteString, oauth: "FACEBOOK", token: AccessToken.current?.tokenString)
                        return completionHandler(obj)
                    }
                }
            }
        }
    }
}

