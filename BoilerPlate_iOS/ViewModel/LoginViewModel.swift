//
//  LoginViewModel.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 22/09/21.
//

import Foundation
import Toast_Swift

class LoginViewModel {
    
    var email : String?
    var password : String?
    var style = ToastStyle()
    var delegate : LoginSignUpDelegate?

    /// Check user enters valid data or not
    /// - Parameters:
    ///   - email: user email
    ///   - password: user password
    func checkValidFields() {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let topVC = GlobalFunctions.topViewController(in: keyWindow?.rootViewController)!
        style.backgroundColor = .red
        guard let userEmail = email , !userEmail.isEmpty else {
            topVC.view.makeToast(UserErrorMessages.emptyEmailMsg,  duration: 3.0, position: .bottom, style: self.style)
            return
        }
        if !Validation().isValidEmail(testStr: userEmail) {
            return topVC.view.makeToast(UserErrorMessages.invalidEmailMsg,  duration: 3.0, position: .bottom, style: self.style)
            
        }
        guard let finalpassword = password , !finalpassword.isEmpty else {
            topVC.view.makeToast(UserErrorMessages.emptyPasswordMsg,  duration: 3.0, position: .bottom, style: self.style)
            return
        }
        if !Validation.sharedInstance.validatePassword(testStr: finalpassword) {
            topVC.view.makeToast(UserErrorMessages.invalidPasswordMsg,  duration: 3.0, position: .bottom, style: self.style)
            return
        }
        
        let  loginObj = LoginRequestModel(user: User(email: userEmail, password: finalpassword))
        self.userLogin(userObj: loginObj)
    }
    
    func userLogin(userObj:LoginRequestModel) {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let topVC = GlobalFunctions.topViewController(in: keyWindow?.rootViewController)!
        style.backgroundColor = .red
        AuthenticationServices.userLogin(loginObj: userObj) { (loginResponse, errorStr) in
            guard let loginDetail = loginResponse else {
                topVC.view.makeToast(errorStr,  duration: 3.0, position: .bottom, style: self.style)
                return
            }
            self.style.backgroundColor = .green
            GlobalFunctions.printToConsole(message: "Login model user login data:- \(loginDetail)")
            topVC.view.makeToast(UserErrorMessages.loginSuccessMsg,  duration: 3.0, position: .bottom, style: self.style)
            self.delegate?.completedProcess(isLogin: true, sMsg: UserErrorMessages.loginSuccessMsg, isError: false)

        }
    }
    
    func sendResetPasswordEmail(email : String?) {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let topVC = GlobalFunctions.topViewController(in: keyWindow?.rootViewController)!
        style.backgroundColor = .red
        guard let userEmail = email , !userEmail.isEmpty else {
            topVC.view.makeToast(UserErrorMessages.emptyEmailMsg,  duration: 3.0, position: .bottom, style: self.style)
            return
        }
        AuthenticationServices.sendResetPasswordEmailApi(userDict: ["email":userEmail]) { (response,errorStr) in
            guard let responseData = response else {
                topVC.view.makeToast(errorStr,  duration: 3.0, position: .bottom, style: self.style)
                return
            }
            self.style.backgroundColor = .green
            GlobalFunctions.printToConsole(message: "Reset passowrd response:- \(responseData)")
            topVC.view.makeToast(UserErrorMessages.resetPasswordLinkSentMsg,  duration: 3.0, position: .bottom, style: self.style)
        }
    }
    
    
    
}

