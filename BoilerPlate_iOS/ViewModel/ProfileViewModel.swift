//
//  ProfileViewModel.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 25/10/21.
//

import Foundation
import Toast_Swift

class ProfileViewModel {
    
    var fname : String?
    var lname : String?
    var style = ToastStyle()
    var delegate : LoginSignUpDelegate?
    var oldPass : String?
    var newPass : String?
    var confrmPass : String?
    /// Check user enters valid data or not
   
    func checkValidFields() {
        
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let topVC = GlobalFunctions.topViewController(in: keyWindow?.rootViewController)!
        style.backgroundColor = .red
        
        guard let fname = fname , !fname.isEmpty else {
            topVC.view.makeToast(UserErrorMessages.emptyFirstNameMsg,  duration: 3.0, position: .bottom, style: self.style)
            return
        }

        guard let lname = lname , !lname.isEmpty else {
            topVC.view.makeToast(UserErrorMessages.emptylastNameMsg,  duration: 3.0, position: .bottom, style: self.style)
            return
        }
       
        let objName = ["first":fname,"last":lname]
        let name = ["name" : objName]
        let updateDict = ["update" : name]
        self.updateProfile(userObj: updateDict)
      
    }
    
    
    
    func updateProfile(userObj:[String:Any]) {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let topVC = GlobalFunctions.topViewController(in: keyWindow?.rootViewController)!
        style.backgroundColor = .red
        AuthenticationServices.updateUserInfoApi(userDict: userObj) { (loginResponse, errorStr) in
            guard let loginDetail = loginResponse else {
                topVC.view.makeToast(errorStr,  duration: 3.0, position: .bottom, style: self.style)
                return
            }
            if let val = loginDetail.nModified  ,val == 1 {
                self.style.backgroundColor = .green
                GlobalFunctions.printToConsole(message: "update model user login data:- \(loginDetail)")
                topVC.view.makeToast(UserErrorMessages.profileUpdatedSuccessMsg,  duration: 3.0, position: .bottom, style: self.style)
                self.delegate?.completedProcess(isLogin: true, sMsg: UserErrorMessages.profileUpdatedSuccessMsg, isError: false)
            } else {
                self.style.backgroundColor = .red
                GlobalFunctions.printToConsole(message: "update model user login data:- \(loginDetail)")
                topVC.view.makeToast(UserErrorMessages.profileUpdatedErrorMsg,  duration: 3.0, position: .bottom, style: self.style)
                self.delegate?.completedProcess(isLogin: true, sMsg: UserErrorMessages.profileUpdatedSuccessMsg, isError: false)

            }
        }
    }
    
    /// Check user enters valid data or not
   
    func checkValidFieldsForUpdatePassword() {
        
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let topVC = GlobalFunctions.topViewController(in: keyWindow?.rootViewController)!
        style.backgroundColor = .red
        
        guard let oldpass = oldPass , !oldpass.isEmpty else {
            topVC.view.makeToast(UserErrorMessages.emptyoldPasswordMsg,  duration: 3.0, position: .bottom, style: self.style)
            return
        }

        guard let newpass = newPass , !newpass.isEmpty else {
            topVC.view.makeToast(UserErrorMessages.emptynewPasswordMsg,  duration: 3.0, position: .bottom, style: self.style)
            return
        }
        guard let confrmpass = confrmPass , !confrmpass.isEmpty else {
            topVC.view.makeToast(UserErrorMessages.emptyCnfrmPasswordMsg,  duration: 3.0, position: .bottom, style: self.style)
            return
        }
        if !Validation().validatePassword(testStr: newpass) {
            return topVC.view.makeToast(UserErrorMessages.invalidPasswordMsg,  duration: 3.0, position: .bottom, style: self.style)
            
        }
        if newpass != confrmpass {
            return topVC.view.makeToast(UserErrorMessages.confirmPasswordMismatch,  duration: 3.0, position: .bottom, style: self.style)
        }
        
        /*{
        "passwordDetails": {
            "currentPassword": "123456789",
            "newPassword": "123456"
        }
    }*/
        let objName = ["currentPassword":oldpass,"newPassword":newpass]
        let updateDict = ["passwordDetails" : objName]
        self.updateProfile(userObj: updateDict)
      
    }
    func updatePassword(userObj:[String:Any]) {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        let topVC = GlobalFunctions.topViewController(in: keyWindow?.rootViewController)!
        style.backgroundColor = .red
        AuthenticationServices.changePasswordApi(userDict: userObj) { (loginResponse, errorStr) in
            guard let loginDetail = loginResponse else {
                topVC.view.makeToast(errorStr,  duration: 3.0, position: .bottom, style: self.style)
                return
            }
            if let val = loginDetail.nModified  ,val == 1 {
                self.style.backgroundColor = .green
                GlobalFunctions.printToConsole(message: "update model user login data:- \(loginDetail)")
                topVC.view.makeToast(UserErrorMessages.profileUpdatedSuccessMsg,  duration: 3.0, position: .bottom, style: self.style)
                self.delegate?.completedProcess(isLogin: true, sMsg: UserErrorMessages.profileUpdatedSuccessMsg, isError: false)
            } else {
                self.style.backgroundColor = .red
                GlobalFunctions.printToConsole(message: "update model user login data:- \(loginDetail)")
                topVC.view.makeToast(UserErrorMessages.profileUpdatedErrorMsg,  duration: 3.0, position: .bottom, style: self.style)
                self.delegate?.completedProcess(isLogin: true, sMsg: UserErrorMessages.profileUpdatedSuccessMsg, isError: false)

            }
        }
    }
    
    
    
    
}
