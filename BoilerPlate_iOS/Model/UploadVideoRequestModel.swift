//
//  UploadVideoRequestModel.swift
//  Skiddoo
//
//  Created by iMac on 22/03/21.
//  Copyright © 2021 Nidhi_Suhagiya. All rights reserved.
//

import Foundation

struct UploadVideoRequestModel : Codable {
    let title : String?
    let description : String?
    let is180 : Bool?
    let is360 : Bool?
    let isPublished : Bool?
    let keywords : [String]?
    var s3response : S3response?
    var genre: String?
    var playbackType: String?
    var videoUrl: String?
    var videoId: String?
    var tags : [String]?
    var duration: Int?
    var vidURL: URL?
}

struct S3response : Codable {
    let video : UploadVideo?
}

struct UploadVideo : Codable {
    let Location : String?
    let Bucket : String?
    let Key : String?
    let eTag : String?

}


