//
//  LoginModel.swift
//  BoilerPlate
//
//  Created by Kalyani on 21/09/21.
//

import Foundation

// MARK: - LoginResponseModel
struct LoginResponseModel : Codable {
    var token: String?
    var user: User?
    var code: Int?
    var message : String?
    enum CodingKeys: String, CodingKey {
        case token = "token"
        case user = "user"
        case code = "code"
        case message = "message"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        token = try values.decodeIfPresent(String.self, forKey: .token)
        user = try values.decodeIfPresent(User.self, forKey: .user)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        message = try values.decodeIfPresent(String.self, forKey: .message)

        //        store header token to use it later on
        if let authToken = token {
            if authToken.count > 0 {
                UserDefaults.standard.set(authToken, forKey: "headerToken")
            }
        }
    }
}

// MARK: - User
struct User : Codable {
    var name: FullName?
    var subscribedToNewsletter: Bool?
    var roles: String?
    var cardTokens: [String]?
    var subscriptionActiveUntil: Int?
    var subscriptionCancellationRequested: Bool?
    var id, email, password, createdAt: String?
    var updatedAt: String?
    var v: Int?
    var fullName: String?
    var isAdmin, isPaidUser: Bool?
    var userID: String?
    
    enum CodingKeys: String, CodingKey {
        case name, subscribedToNewsletter, roles, cardTokens, subscriptionActiveUntil, subscriptionCancellationRequested
        case id = "_id"
        case email, password, createdAt, updatedAt
        case v = "__v"
        case fullName, isAdmin, isPaidUser
        case userID = "id"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(FullName.self , forKey: .name)
        roles = try values.decodeIfPresent(String.self, forKey: .roles)
        subscribedToNewsletter = try values.decodeIfPresent(Bool.self, forKey: .subscribedToNewsletter)
        cardTokens =  try values.decodeIfPresent([String].self, forKey: .cardTokens)
        subscriptionActiveUntil = try values.decodeIfPresent(Int.self, forKey: .subscriptionActiveUntil)
        subscriptionCancellationRequested = try values.decodeIfPresent(Bool.self, forKey: .subscriptionCancellationRequested)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        createdAt = try values.decodeIfPresent(String.self, forKey: .createdAt)
        updatedAt = try values.decodeIfPresent(String.self, forKey: .updatedAt)
        fullName = try values.decodeIfPresent(String.self, forKey: .fullName)
        userID = try values.decodeIfPresent(String.self, forKey: .userID)
        v = try values.decodeIfPresent(Int.self, forKey: .v)
        isAdmin = try values.decodeIfPresent(Bool.self, forKey: .isAdmin)
        isPaidUser = try values.decodeIfPresent(Bool.self, forKey: .isPaidUser)
        
    }
    
    init(email : String?, password : String?, name: FullName?) {
        self.email = email
        self.password = password
        self.name = name
    }
    
    init(email : String?, password : String?) {
        self.email = email
        self.password = password
    }
}

// MARK: - Name
struct FullName : Codable {
    var first, last: String?
    
    enum CodingKeys: String, CodingKey {
        case first,last
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        first = try values.decodeIfPresent(String.self , forKey: .first)
        last = try values.decodeIfPresent(String.self , forKey: .last)

    }
    init(first : String?, last : String?) {
        self.first = first
        self.last = last
    }
    
}



// MARK: - LoginRequestModel
struct LoginRequestModel : Codable  {
    var user: User?
    enum CodingKeys: String, CodingKey {
        case user
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user = try values.decodeIfPresent(User.self , forKey: .user)

    }
    init(user : User?) {
        self.user = user
       
    }
}


struct SocialLoginRequestModel: Codable {
    
    var name: FullName?
    var email: String?
    var profile: String?
    var oauth: String?
    var token: String?
 
    
    init(name : FullName?, email : String?, profile : String?, oauth: String?, token: String?) {
        self.name = name
        self.email = email
        self.profile = profile
        self.oauth = oauth
        self.token = token
    }

    init(name : FullName?, oauth : String?, token : String?, email: String) {
        self.name = name
        self.oauth = oauth
        self.token = token
        self.email = email
    }
    init(name : FullName?, oauth : String?, email: String) {
        self.name = name
        self.oauth = oauth
        self.email = email
    }
    init(oauth : String?, token : String?) {
        self.oauth = oauth
        self.token = token
    }

    init(email: String?,oauth: String?) {
        self.email = email
        self.oauth = oauth

    }
    
    
}
struct ForgotPasswordResponseModel: Decodable {
    let statusCode: Int?
    let message: String?
    
    enum CodingKeys: String, CodingKey {
        case statusCode = "status"
        case message = "message"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        statusCode = try values.decodeIfPresent(Int.self, forKey: .statusCode)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }
}
struct UpdateRequestModel {
    var update: Update?
    init(update : Update?) {
        self.update = update
    }
}

// MARK: - Update
struct Update {
    var name: FullName?
}
// MARK: - UpdateResponsetModel
struct UpdateResponsetModel : Codable {
    var nModified: Int?
 
    enum CodingKeys: String, CodingKey {
        case nModified = "nModified"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        nModified = try values.decodeIfPresent(Int.self, forKey: .nModified)
    }
}


