//
//  FollowVC.swift
//  Skiddoo
//
//  Created by Kalyani on 24/05/21.
//  Copyright © 2021 Nidhi_Suhagiya. All rights reserved.
//

import UIKit
import Foundation
import PureLayout
class FollowScreenVC: UIViewController {
    
    private var tableView: UITableView!
    private var userArr = ["Test1","Test2","Test3","Test4"]
    private let minInterval = 0.02
    var userID : String?
    var videoID : String?
    var loggedId : String?
    var currentPage = 1
    var isLoadingList : Bool = false
    var hasMoreData: Bool = true
    
    lazy var noDataFoundLbl: UILabel = {
        let noDataLbl = UILabel()
        noDataLbl.text = "No records."
        noDataLbl.textColor = UIColor.white
        noDataLbl.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        noDataLbl.textAlignment = .center
        noDataLbl.isHidden = true
        return noDataLbl
    }()
    
    lazy var likesTitleLbl: UILabel = {
        let lbl = UILabel()
        lbl.text = "Likes"
        lbl.textColor = UIColor.white
        lbl.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        lbl.textAlignment = .center
        lbl.isHidden = true
        return lbl
    }()
    
  
    
    var bForFollower : Bool = false
    var bForLikes : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavBar()
        self.setUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
      
        self.loadTableData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    //    #MARK:- set navigation bar
    private func setNavBar() {
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = false
        if bForFollower {
            self.navigationItem.title = "Followers"
        } else {
            self.navigationItem.title = "Following"
        }
    }
    //    set up user interface
    private func setUI() {
        self.addLikeLbl()
        self.addTableView()
        self.addNodataLbl()
    }
    
   
    
    
    ///    Add table view
    private func addTableView() {
        tableView = UITableView(frame: .zero, style: .grouped)
        tableView.backgroundColor = .clear//ThemeColors.bgColor
        tableView.separatorStyle = .none
        self.view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        tableView.register(FollowCell.self, forCellReuseIdentifier: "FollowUser")
        tableView.autoPinEdge(.leading, to: .leading, of: self.view)
        tableView.autoPinEdge(.trailing, to: .trailing, of: self.view)
        tableView.autoPinEdge(.bottom, to: .bottom, of: self.view)
        tableView.autoPinEdge(.top, to: .bottom, of: self.likesTitleLbl, withOffset: 0)
    }
    
    /// Add no data found error message
    private func addNodataLbl() {
        self.view.addSubview(noDataFoundLbl)
        noDataFoundLbl.isHidden = true
        noDataFoundLbl.center = self.view.center//tableView.center
    }
    
    /// Add like title label
    private func addLikeLbl() {
        self.view.addSubview(likesTitleLbl)
        likesTitleLbl.autoPinEdge(.leading, to: .leading, of: self.view)
        likesTitleLbl.autoPinEdge(.trailing, to: .trailing, of: self.view)
        likesTitleLbl.autoPinEdge(.top, to: .top, of: self.view, withOffset: 0)
        if self.bForLikes {
            likesTitleLbl.isHidden = false
            likesTitleLbl.autoSetDimensions(to: CGSize(width:250 , height: 40))
            
        } else {
            likesTitleLbl.isHidden = true
            likesTitleLbl.autoSetDimensions(to: CGSize(width:250 , height: 0))
            
        }
    }
    
}

//#MARK:- Tableview delegate and datasource methods
extension FollowScreenVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FollowUser", for: indexPath) as! FollowCell
        cell.isFollower = self.bForFollower
        cell.loggedInUserID = self.loggedId
        cell.userInfo = userArr[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

//#MARK:- Scrollview delegate
extension FollowScreenVC: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){
            self.loadTableData()
        }
    }
    
    //    load next page data
    func loadTableData() {
       
    }
}
