//
//  UpdateProfileVc.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 25/10/21.
//

import UIKit

class UpdateProfileVc: UIViewController {

    @IBOutlet weak var txtFname: UITextField!
    @IBOutlet weak var txtLname: UITextField!
    var objModel : ProfileViewModel?
    @IBOutlet weak var btnChangePassword: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let userInfo = userDetail {
            if let user = userInfo.fullName {
                let arrName = user.components(separatedBy: " ")
                if arrName.count > 0 {
                    txtFname.text = arrName.first
                    txtLname.text = arrName.last
                }
            }
        }

        // Do any additional setup after loading the view.
    }
    @IBAction func btnSaveTapped(_ sender: Any) {
        objModel = ProfileViewModel()
        objModel?.fname = txtFname.text
        objModel?.lname = txtLname.text
       // objModel?.delegate = self
        objModel?.checkValidFields()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
