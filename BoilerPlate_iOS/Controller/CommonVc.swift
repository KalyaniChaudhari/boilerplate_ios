//
//  CommonVc.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 29/10/21.
//

import UIKit

class CommonVc: UIViewController {
    
    var strNavigationTitle : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        // Do any additional setup after loading the view.
    }
    private func setNavigationBar() {
        self.navigationItem.title = strNavigationTitle
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.hidesBackButton = false
       
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
