//
//  UpdatePasswordVC.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 29/10/21.
//

import UIKit

class UpdatePasswordVC: UIViewController {
    
    

    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    var objModel : ProfileViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnSaveTapped(_ sender: Any) {
        objModel = ProfileViewModel()
        objModel?.newPass = txtNewPassword.text
        objModel?.oldPass = txtOldPassword.text
        objModel?.confrmPass = txtConfirmPassword.text
         objModel?.delegate = self
        objModel?.checkValidFieldsForUpdatePassword()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    private func clearData() {
        txtOldPassword.text = ""
        txtNewPassword.text = ""
        txtConfirmPassword.text = ""
    }
}
extension UpdatePasswordVC: LoginSignUpDelegate {
    func completedProcess(isLogin: Bool, sMsg: String, isError: Bool) {
        self.clearData()
    }
}
