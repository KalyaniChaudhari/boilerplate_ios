//
//  SocialSignUpVc.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 29/09/21.
//

import UIKit
import GoogleSignIn
import AuthenticationServices
class SocialSignUpVc: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance()?.delegate = self

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnFacebookLoginTapped(_ sender: Any) {
        FacebookSignUp.facebookLogIn(currentVC: self) { (userObj) in
            self.callSocialLoginSignUpApi(loginObj: userObj)
        }
    }
    @IBAction func btnAppleLoginTapped(_ sender: Any) {
        self.appleSignIn()
    }
    @IBAction func btnGoogleLoginTapped(_ sender: Any) {
        GIDSignIn.sharedInstance()?.signOut()
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    /// Apple sign in:- open ASAuthorizationController
    private func appleSignIn() {
        let appleProvider = ASAuthorizationAppleIDProvider()
        let request = appleProvider.createRequest()
        request.requestedScopes = [.email, .fullName]
        
        let authController = ASAuthorizationController(authorizationRequests: [request])
        authController.delegate = self
        authController.presentationContextProvider = self
        authController.performRequests()
    }
    private func redirectToMainScreenOnLoginSignup() {
        if let destinationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeScreenVc") as? HomeScreenVc {
            destinationVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(destinationVC, animated: true)
        }
       
    }
    /// Call social login and sign up api
    /// - Parameter loginObj: SocialLoginRequestModel object which has user info and oauth type
     func callSocialLoginSignUpApi(loginObj: SocialLoginRequestModel) {
        AuthenticationServices.SocialSignUpLoginApi(loginObj: loginObj, isLogin: false) { (loginSignUpInfo) in
            GlobalFunctions.printToConsole(message: "Social loginSignUpInfo:- \(loginSignUpInfo)")
            //check if email already exits
            if loginSignUpInfo?.code == 409 { //Email Already exits
                //Navigate to Home Screen
            } else {
                
                if let responseData = loginSignUpInfo {
                    //            Store user data on success
                    if let authToken = responseData.token {
                        UserDefaults.standard.set(authToken, forKey: "headerToken")
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        GlobalFunctions.showErrorSuccessDialog(isError: true, errorMessage: "Failed to Sign up! Please try again", vc: self) {}
                    }
                }
            }
            //Navigate to Home Screen
            self.redirectToMainScreenOnLoginSignup()
            
        } onFailure: { (error) in
            DispatchQueue.main.async {
                GlobalFunctions.showErrorSuccessDialog(isError: true, errorMessage: error ?? "Failed to Login! Please try again", vc: self) {}
            }
        }
    }
}
//#MARK:- Google sign in delegates
extension SocialSignUpVc: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard let userData = user else {
            GlobalFunctions.printToConsole(message: "Google sign in is failed:- \(error)")
            return
        }
        
       
      //  let profileStr: URL? = userData.profile.hasImage ? userData.profile.imageURL(withDimension: 200) : nil
      //  let obj = SocialLoginRequestModel(name: userData.profile.name, email: userData.profile.email, profile: profileStr?.absoluteString, oauth: "GOOGLE", token: userData.authentication.idToken)
        let fname = FullName(first: userData.profile.givenName, last: userData.profile.familyName)
        let obj = SocialLoginRequestModel(name:fname , oauth: "GOOGLE", email:  userData.profile.email)
        self.callSocialLoginSignUpApi(loginObj: obj)
        //send token and user data on server
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        GlobalFunctions.printToConsole(message: "Disconnection error")
    }
    
    
}
//#MARK:- Apple sign in delegates
extension SocialSignUpVc: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        guard let error = error as? ASAuthorizationError else {
            return
        }
        switch error.code {
        case .canceled:
            GlobalFunctions.printToConsole(message: "Canceled")
        case .unknown:
            GlobalFunctions.printToConsole(message: "Unknown")
        case .invalidResponse:
            GlobalFunctions.printToConsole(message: "Invalid Respone")
        case .notHandled:
            GlobalFunctions.printToConsole(message: "Not handled")
        case .failed:
            GlobalFunctions.printToConsole(message: "Failed")
        @unknown default:
            GlobalFunctions.printToConsole(message: "Default")
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        guard let authToken = authorization.credential as? ASAuthorizationAppleIDCredential else {
            return
        }
        
        if let appIdToken = authToken.identityToken, let token = String(data: appIdToken, encoding: .utf8) {
            //            You can only retrieve the full name and email for the first time. If these are the required information for your registration process, make sure you save it before begin your registration process and remove it once your registration is confirmed.
            var userName = ""
            var userEmail = ""
            if let fullName = authToken.fullName, let email = authToken.email {
                userName = fullName.givenName! + " " + (fullName.familyName ?? "")
                userEmail = email
            }
            let name = FullName(first: authToken.fullName?.givenName, last: authToken.fullName?.familyName)
            let obj = SocialLoginRequestModel(name: name, oauth: "APPLE", email: userEmail)
         //   let obj = SocialLoginRequestModel(oauth: "APPLE", token: token)//SocialLoginRequestModel(name: userName, oauth: "APPLE", token: token, email: userEmail)
           self.callSocialLoginSignUpApi(loginObj: obj)
        } else {
            GlobalFunctions.printToConsole(message: "Unable to serialize token string from data: \(authToken.debugDescription)")
        }
    }
    
    
}

