//
//  FollowCell.swift
//  Skiddoo
//
//  Created by Kalyani on 24/05/21.
//  Copyright © 2021 Nidhi_Suhagiya. All rights reserved.
//



import Foundation
import UIKit
import PureLayout

class FollowCell: UITableViewCell {
    
    private lazy var profileImgView: UIImageView = {
        var img = UIImageView()
        img.contentMode = .scaleToFill
        img.clipsToBounds = true
        let imgHeight:CGFloat = 45
        img.autoSetDimensions(to: CGSize(width: imgHeight, height: imgHeight))
        img.layer.cornerRadius = CGFloat(imgHeight/2)
        img.layer.borderColor = UIColor.white.cgColor
        img.layer.borderWidth = 2
        return img
    }()
    
    private lazy var userNameLbl: UILabel = {
        var lbl = UILabel()
        lbl.textAlignment = .left
        lbl.textColor = .white
        lbl.font = UIFont.systemFont(ofSize: 20.0)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 14.0
        lbl.numberOfLines = 2
        return lbl
    }()
    
    private lazy var handleLbl: UILabel = {
        var lbl = UILabel()
        lbl.textAlignment = .left
        lbl.textColor =  UIColor.white.withAlphaComponent(0.4)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.minimumScaleFactor = 14.0
        lbl.numberOfLines = 2
        return lbl
    }()
    
    private lazy var followBtn: UIButton = {
        var btn = UIButton()
        btn.setTitleColor(.white, for: .normal)
        self.clipsToBounds = true
        let fheight = CGFloat(35.0)
        btn.autoSetDimensions(to: CGSize(width: 100, height: fheight))
        btn.setTitle("Follow", for: .normal)
        btn.setTitle("Following", for: .selected)
        btn.layer.cornerRadius = fheight / 2
        
        btn.addTarget(self, action: #selector(btnFollowtapped(_:)), for: .touchUpInside)
        return btn
    }()
    
   
    var isFollower : Bool = false
    var loggedInUserID : String?
    var isForTagList : Bool = false
    var isTagAdded : Bool = false
    
    //    Set username and userprofile value
    var userInfo: String? {
        didSet {
            
            userNameLbl.text = userInfo
            
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        self.selectionStyle = .none
        self.setUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        fatalError("init(coder:) has not been implemented")
    }
    
    //    set user interface
    func setUI() {
        self.contentView.addSubview(profileImgView)
        profileImgView.autoAlignAxis(.horizontal, toSameAxisOf: self.contentView)
        profileImgView.autoPinEdge(.leading, to: .leading, of: self.contentView, withOffset: 15)
        
        let stackView = UIStackView()
        stackView.spacing = 0
        stackView.axis = .vertical
        self.contentView.addSubview(stackView)
        stackView.addArrangedSubview(userNameLbl)
        stackView.addArrangedSubview(handleLbl)
        
        self.contentView.addSubview(followBtn)
        stackView.autoPinEdge(.leading, to: .trailing, of: self.profileImgView, withOffset: 15)
        stackView.autoPinEdge(.trailing, to: .leading, of: self.followBtn, withOffset: -15)
        stackView.autoAlignAxis(.horizontal, toSameAxisOf: self.profileImgView)
        
        followBtn.autoAlignAxis(.horizontal, toSameAxisOf: stackView)
        followBtn.autoPinEdge(.leading, to: .trailing, of: stackView, withOffset: 15)
        followBtn.autoPinEdge(.trailing, to: .trailing, of: self.contentView, withOffset: -10)
        
        
    }
    
    @objc func btnFollowtapped(_ sender : UIButton) {
        self.followBtn.isSelected = !self.followBtn.isSelected
       
    }
    
    
}

