//
//  LoginVC.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 22/09/21.
//

import UIKit
import GoogleSignIn
import AuthenticationServices

class LoginVC: UIViewController {
   

    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    var objModel : LoginViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance()?.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnLoginTapped(_ sender: Any) {
        objModel = LoginViewModel()
        objModel?.email = txtEmail.text
        objModel?.password = txtPassword.text
        objModel?.delegate = self
        objModel?.checkValidFields()
    }
    
    
    @IBAction func btnFacebookLoginTapped(_ sender: Any) {
        FacebookSignUp.facebookLogIn(currentVC: self) { (userObj) in
            self.callSocialLoginSignUpApi(loginObj: userObj)
        }
    }
    @IBAction func btnAppleLoginTapped(_ sender: Any) {
        self.appleSignIn()
    }
    @IBAction func btnGoogleLoginTapped(_ sender: Any) {
        GIDSignIn.sharedInstance()?.signOut()
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance()?.signIn()
    }
    private func clearData() {
        txtEmail.text = ""
        txtPassword.text = ""
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    /// Apple sign in:- open ASAuthorizationController
    private func appleSignIn() {
        let appleProvider = ASAuthorizationAppleIDProvider()
        let request = appleProvider.createRequest()
        request.requestedScopes = [.email, .fullName]
        
        let authController = ASAuthorizationController(authorizationRequests: [request])
        authController.delegate = self
        authController.presentationContextProvider = self
        authController.performRequests()
    }

    /// Call social login and sign up api
    /// - Parameter loginObj: SocialLoginRequestModel object which has user info and oauth type
     func callSocialLoginSignUpApi(loginObj: SocialLoginRequestModel) {
        let obj = SocialLoginRequestModel(email: loginObj.email, oauth: loginObj.oauth)
        AuthenticationServices.SocialSignUpLoginApi(loginObj: obj, isLogin: true) { (loginSignUpInfo) in
            GlobalFunctions.printToConsole(message: "Social loginSignUpInfo:- \(loginSignUpInfo)")
            if loginSignUpInfo == nil {
                DispatchQueue.main.async {
                    GlobalFunctions.showErrorSuccessDialog(isError: true, errorMessage: "Failed to Login! Please try again", vc: self) {}
                }
            }
            
            //            post notification to change nav bar on successful logged in
           self.redirectToMainScreenOnLoginSignup()
        } onFailure: { (error) in
            DispatchQueue.main.async {
                GlobalFunctions.showErrorSuccessDialog(isError: true, errorMessage: error ?? "Failed to Login! Please try again", vc: self) {}
            }
        }
    }
    
}
extension LoginVC:  LoginSignUpDelegate {
    func completedProcess(isLogin: Bool, sMsg: String, isError: Bool) {
        self.redirectToMainScreenOnLoginSignup()
        self.clearData()
    }
}
//#MARK:- Google sign in delegates
extension LoginVC: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard let userData = user else {
            GlobalFunctions.printToConsole(message: "Google sign in is failed:- \(error)")
            return
        }
        
       
      //  let profileStr: URL? = userData.profile.hasImage ? userData.profile.imageURL(withDimension: 200) : nil
      //  let obj = SocialLoginRequestModel(name: userData.profile.name, email: userData.profile.email, profile: profileStr?.absoluteString, oauth: "GOOGLE", token: userData.authentication.idToken)
        let fname = FullName(first: userData.profile.givenName, last: userData.profile.familyName)
        let obj = SocialLoginRequestModel(name:fname , oauth: "GOOGLE", email:  userData.profile.email)
        self.callSocialLoginSignUpApi(loginObj: obj)
        //send token and user data on server
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        GlobalFunctions.printToConsole(message: "Disconnection error")
    }
    
    ///    Redirect user back to the main screen after successful sign up or login
    private func redirectToMainScreenOnLoginSignup() {
        if let destinationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeScreenVc") as? HomeScreenVc {
            destinationVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(destinationVC, animated: true)
        }
       
       
    }
    
   
    
}
//#MARK:- Apple sign in delegates
extension LoginVC: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        // Handle error.
        guard let error = error as? ASAuthorizationError else {
            return
        }
        switch error.code {
        case .canceled:
            GlobalFunctions.printToConsole(message: "Canceled")
        case .unknown:
            GlobalFunctions.printToConsole(message: "Unknown")
        case .invalidResponse:
            GlobalFunctions.printToConsole(message: "Invalid Respone")
        case .notHandled:
            GlobalFunctions.printToConsole(message: "Not handled")
        case .failed:
            GlobalFunctions.printToConsole(message: "Failed")
        @unknown default:
            GlobalFunctions.printToConsole(message: "Default")
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        guard let authToken = authorization.credential as? ASAuthorizationAppleIDCredential else {
            return
        }
        
        if let appIdToken = authToken.identityToken, let token = String(data: appIdToken, encoding: .utf8) {
            //            You can only retrieve the full name and email for the first time. If these are the required information for your registration process, make sure you save it before begin your registration process and remove it once your registration is confirmed.
            var userName = ""
            var userEmail = ""
            if let fullName = authToken.fullName, let email = authToken.email {
                userName = fullName.givenName! + " " + (fullName.familyName ?? "")
                userEmail = email
            }
            let name = FullName(first: authToken.fullName?.givenName, last: authToken.fullName?.familyName)
            let obj = SocialLoginRequestModel(name: name, oauth: "APPLE", email: userEmail)
         //   let obj = SocialLoginRequestModel(oauth: "APPLE", token: token)//SocialLoginRequestModel(name: userName, oauth: "APPLE", token: token, email: userEmail)
           self.callSocialLoginSignUpApi(loginObj: obj)
        } else {
            GlobalFunctions.printToConsole(message: "Unable to serialize token string from data: \(authToken.debugDescription)")
        }
    }
    
    
}
