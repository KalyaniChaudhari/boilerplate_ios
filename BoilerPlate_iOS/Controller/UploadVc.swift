//
//  UploadVc.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 28/10/21.
//

import UIKit

class UploadVc: UIViewController {
    
    @IBOutlet weak var s3UrlLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.progressView.progress = Float(0.0)
    }
    
    @IBAction func tapUploadImage(_ sender: Any) {
        guard let image = UIImage(named: "ImageFile.jpg") else { return } //1
            AWSS3Manager.shared.uploadImage(image: image, progress: {[weak self] ( uploadProgress) in
                
                guard let strongSelf = self else { return }
                strongSelf.progressView.progress = Float(uploadProgress)//2
                
            }) {[weak self] (uploadedFileUrl, error) in
                
                guard let strongSelf = self else { return }
                if let finalPath = uploadedFileUrl as? String { // 3
                    strongSelf.s3UrlLabel.text = "Uploaded file url: " + finalPath
                    strongSelf.progressView.progress = Float(0)
                } else {
                    print("\(String(describing: error?.localizedDescription))") // 4
                }
            }
    }
    
    @IBAction func tapUploadVideo(_ sender: Any) {
           guard let path = Bundle.main.path(forResource: "VideoFile", ofType: "mp4") else { return }
            let videoUrl = URL(fileURLWithPath:path)
            AWSS3Manager.shared.uploadVideo(videoUrl: videoUrl, progress: { [weak self] (progress) in
                
                guard let strongSelf = self else { return }
                strongSelf.progressView.progress = Float(progress)
                
            }) { [weak self] (uploadedFileUrl, error) in
                
                guard let strongSelf = self else { return }
                if let finalPath = uploadedFileUrl as? String {
                    strongSelf.s3UrlLabel.text = "Uploaded file url: " + finalPath
                    strongSelf.progressView.progress = Float(0)
                } else {
                    print("\(String(describing: error?.localizedDescription))")
                }
            }
    }
    
    @IBAction func tapUploadAudio(_ sender: Any) {
        guard let path = Bundle.main.path(forResource: "AudioFile", ofType: "mp3") else { return }
        let audioUrl = URL(fileURLWithPath: path)
           AWSS3Manager.shared.uploadAudio(audioUrl: audioUrl, progress: { [weak self] (progress) in
               
               guard let strongSelf = self else { return }
               strongSelf.progressView.progress = Float(progress)
               
           }) { [weak self] (uploadedFileUrl, error) in
               
               guard let strongSelf = self else { return }
               if let finalPath = uploadedFileUrl as? String {
                   strongSelf.s3UrlLabel.text = "Uploaded file url: " + finalPath
                strongSelf.progressView.progress = Float(0)
               } else {
                   print("\(String(describing: error?.localizedDescription))")
               }
           }
    }
    
    @IBAction func tapUploadFile(_ sender: Any) {
        guard let path = Bundle.main.path(forResource: "TextFile", ofType: "rtf") else { return }
        let audioUrl = URL(fileURLWithPath:path)
           AWSS3Manager.shared.uploadAudio(audioUrl: audioUrl, progress: { [weak self] (progress) in
               
               guard let strongSelf = self else { return }
               strongSelf.progressView.progress = Float(progress)
               
           }) { [weak self] (uploadedFileUrl, error) in
               
               guard let strongSelf = self else { return }
               if let finalPath = uploadedFileUrl as? String {
                   strongSelf.s3UrlLabel.text = "Uploaded file url: " + finalPath
                strongSelf.progressView.progress = Float(0)
               } else {
                   print("\(String(describing: error?.localizedDescription))")
               }
           }
    }
}
