//
//  SignUpVC.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 22/09/21.
//

import UIKit

class SignUpVC: UIViewController {

    @IBOutlet weak var txtFname: UITextField!
    @IBOutlet weak var txtLname: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    var objSignModel : SignUpViewModel?
    var currentUserDetail: SocialLoginRequestModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnRegisterClicked(_ sender: Any) {
        objSignModel = SignUpViewModel()
        objSignModel?.emailStr = self.txtEmail?.text
        objSignModel?.fname = self.txtFname?.text
        
        objSignModel?.lname = self.txtLname?.text
        
        objSignModel?.password = self.txtPassword?.text
        objSignModel?.delegate = self
        objSignModel?.confirmpass = self.txtConfirmPassword?.text
        objSignModel?.checkValidFields()
        
    }
    private func clearData() {
        txtEmail.text = ""
        txtPassword.text = ""
        txtFname.text = ""
        txtLname.text = ""
        txtConfirmPassword.text = ""
    }
    private func redirectToMainScreenOnLoginSignup() {
        if let destinationVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeScreenVc") as? HomeScreenVc {
            destinationVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(destinationVC, animated: true)
        }
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    private func callSocialLoginSignUpApi(loginObj: SocialLoginRequestModel) {
        AuthenticationServices.SocialSignUpLoginApi(loginObj: loginObj, isLogin: false) { (loginSignUpInfo) in
            GlobalFunctions.printToConsole(message: "Social loginSignUpInfo:- \(String(describing: loginSignUpInfo))")
            //check if email already exits
            if loginSignUpInfo?.code == 409 { //Email Already exits
                //Navigate to Home Screen
            } else {
                
                if let responseData = loginSignUpInfo {
                    //            Store user data on success
                    if let authToken = responseData.token {
                        UserDefaults.standard.set(authToken, forKey: "headerToken")
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        GlobalFunctions.showErrorSuccessDialog(isError: true, errorMessage: "Failed to Sign up! Please try again", vc: self) {}
                    }
                }
            }
            //Navigate to Home Screen
            self.redirectToMainScreenOnLoginSignup()
            
        } onFailure: { (error) in
            DispatchQueue.main.async {
                GlobalFunctions.showErrorSuccessDialog(isError: true, errorMessage: error ?? "Failed to Sign up! Please try again", vc: self) {}
            }
        }
    }

}
extension SignUpVC:  LoginSignUpDelegate {
    func completedProcess(isLogin: Bool, sMsg: String, isError: Bool) {
        self.redirectToMainScreenOnLoginSignup()
        self.clearData()
    }
}
