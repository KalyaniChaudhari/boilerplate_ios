//
//  HomeScreenVc.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 25/10/21.
//

import UIKit

class HomeScreenVc: UIViewController {

    @IBOutlet weak var lblwelcomeuser: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        if let userInfo = userDetail {
            if let user = userInfo.name {
                lblwelcomeuser.text = "Welcome \(user.first ?? "")"
            }
        }
    }

    @IBAction func btnLogoutTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure, do you want to logout?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
            GlobalFunctions.removeStoredUserData()
            self.navigationController?.popViewController(animated: false)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: nil))
        self.present(alert, animated: false, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
