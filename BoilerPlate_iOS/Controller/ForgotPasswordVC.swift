//
//  ForgotPasswordVC.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 03/10/21.
//

import UIKit

class ForgotPasswordVC: UIViewController {
    
    @IBOutlet weak var txtEmail: UITextField!
    var objModel : LoginViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnEmailTapped(_ sender: Any) {
        objModel = LoginViewModel()
        objModel?.sendResetPasswordEmail(email: txtEmail.text)
            
        
    }
    private func clearData() {
        txtEmail.text = ""
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
