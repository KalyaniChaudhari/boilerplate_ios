//
//  CommonExtension.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 22/09/21.
//

import Foundation
extension Encodable {
    
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}
