//
//  Constants.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 22/09/21.
//

import Foundation
import UIKit
import Alamofire

class Config {
    static var endpoint: String  {
        get {
            
            if let path = Bundle.main.path(forResource: "Info", ofType: "plist") {
                if let dic = NSDictionary(contentsOfFile: path) {
                    return dic["ENDPOINT_URL"] as? String ?? ""
                }
            }
            return ""
        }
    }
}

struct ApiEndpoints {
    
    static let loadVideoUrl = "https://content.uplynk.com/"
    
    #if DEBUG
    static let domainUrl = Config.endpoint
    #else
    static let domainUrl = Config.endpoint
    #endif
    static let apiUrl = domainUrl + "api/"
    
   
    static let loginApi = apiUrl + "auth/login"
    static let socialLoginApi = apiUrl + "auth/loginOauth"
    static let socialSignupApi = apiUrl + "auth/registerOauth"
    
    static let signUpApi = apiUrl + "auth/register"
    static let sendResetEmailApi = apiUrl + "user/send-reset-email"
    static let checkEmailExistApi = apiUrl + "auth/check-email"
    static let checkHandleExistApi = apiUrl + "auth/check-handle"
    static let checkPhoneExistApi = apiUrl + "auth/check-phone"
    static let loggedInUserDetailApi = apiUrl + "auth/me"
    
    static let updateUserInfoApi = apiUrl + "user/update-profile"
    
    static let changePasswordApi = apiUrl +  "user/changePassword"
}





struct deviceConstants {
    static let deviceHeight = UIScreen.main.bounds.height
    static let deviceWidth = UIScreen.main.bounds.width
}

struct UserErrorMessages {
    
    static let invalidEmailMsg = "Please enter valid e-mail."
    static let invalidPasswordMsg  = "Password must be at least of 8 character and it should contain at least 1 number and 1 upper case letter."
    static let invalidPhoneNumberMsg  = "Please enter valid phone number."
    static let passwordMismatchErrorMsg  = "Password mismatch."
    static let invalidHandleMsg = "Handle must be atleast 4 characters."
    static let emptyEmailMsg = "Please enter e-mail."
    static let emptyPasswordMsg = "Please enter password."
    static let emptyFirstNameMsg = "Please enter first name."
    static let emptylastNameMsg = "Please enter last name."
    static let emptyAddrMsg = "Please enter your billing address."
    static let emptyShippingAddrMsg = "Please enter your shipping address."
    static let emptyCityMsg = "Please enter city."
    static let emptyStateMsg = "Please enter state."
    static let emptyCountryMsg = "Please enter country."
    static let emptyZipCodeMsg = "Please enter zip code."
    static let emptyCardNumberMsg = "Please enter card number."
    static let emptyExpDateMsg = "Please enter card expiration date."
    static let emptyCvvMsg = "Please enter security code(CVV)."
    static let emptyPhoneNumber = "Please enter phone number."
    static let emptyoldPasswordMsg = "Please enter old password."
    static let emptynewPasswordMsg = "Please enter new password."
    static let emptyCnfrmPasswordMsg = "Please enter confirm password."
    //    static let emptyCancelSubscriptionReasonMsg = "Please write your reason."
    
    static let invalidCardNumberMsg = "Please enter valid card number."
    static let invalidCVVMsg = "Please enter valid cvv."
    static let invalidExpDateMsg = "Please enter valid expiration date."
    
    static let selectGenderMsg = "Please select your gender."
    static let selectAgeMsg = "Please select your age."
    static let selectBirthDateMsg = "Please select your birthDate."
    
    //    success messages
    static let loginSuccessMsg = "Login successfully."
    static let signUpSuccessMsg = "Signup successfully."
    static let resetPasswordLinkSentMsg = "An email has been sent successfully. Please check your email to reset your password."
    static let paymentSucceedMsg = "Payment has been done successfully. You will be charge on "
    static let inforUpdatedSuccessMsg = "Information updated successfully."
    static let paymentInfoUpdatedSuccessMsg = "Payment information updated successfully."
    static let profileUpdatedSuccessMsg = "Profile has been updated successfully."
    static let profileUpdatedErrorMsg = "Profile updatation failed."
    static let passwordUpdatedSuccessMsg = "Password has been updated successfully."
    static let passwordUpdatedErrorMsg = "Password updatation failed."
    
    static let noDataFoundMsg = "No data found."
    static let noNetworkErrMsg = "There is no internet connection."
    
    static let serverError = "Oops! Something went wrong.\nClick here to try again"
    static let successfulPurchaseGogglesMsg = "Congratulations! Your order has been placed successfully."
    
    static let phoneExistErrorMsg = "Phone number already in use."
    static let emailExistErrorMsg = "Email already in use."
    static let handlesExistErrorMsg = "Handles already in use."
    
static let confirmPasswordMismatch = "Password & Confirm Password mismatch"
    static let emptyConfirmPasswordMsg = "Please enter confirm password."

}

struct KeyConstant {
    static let kBio = "bio"
    static let kProfilePicture = "profilePicture"
    static let storyboardVideoRecord = UIStoryboard(name: "VideoRecordStoryboard", bundle: nil)
    static let kFileVideoFeeds = "FileVideoFeeds"
    static let kSharing = "Sharing"
    static let kLastUploadVideoURL = "LastUploadVideoURL"
    static let kReuploadLastVideo = "ReuploadlastVideo"
    static let kLastUploadCommentVideoURL = "LastUploadCommentVideoURL"
    static let kReuploadLastCommentVideo = "ReuploadlastCommentVideo"
    static let kLastUploadObject = "LastUploadObject"
    static let kResumeUploadMsg = "No internet. Please reconnect"
    static let kPhone  = "phone"
    static let KRemoteNotifciations = "RemoteNotifications"
    
    static let S3_BUCKET_REGION = "us-east-2"
    static let S3_BUCKET_NAME = "boilerplate-s3-upload"
    static let S3_USER_KEY = "AKIAWJO53WPWBCB2SMWH"
    static let S3_USER_SECRET = "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"
    
}
struct Objects {
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    static let appName = AppConfiguration.AppName //Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
    static let appRefLink = "XXX invited you to \(AppConfiguration.AppName):- \(AppConfiguration.AppLink)"
}

var tokenHeader : HTTPHeaders {
    if let token = UserDefaults.standard.string(forKey: "headerToken") {
        return ["Authorization" : token]//"Bearer \(token)"]
    }else {
        return [:]
    }
}

var userDetail: User? {
    let userData = JsonConvertor.convertStrToObj()
    return userData
}
//var userVideos = [UserVideoFeedModel]()

let loadingTitle = "Loading..."
let int_yellow : UInt = 0xFE5E8C
 


var statusBarHeight: CGFloat {
    var top: CGFloat = 0
    if #available(iOS 13.0, *) {
        top += UIApplication.shared.windows.first?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
    } else {
        top += UIApplication.shared.statusBarFrame.height
    }
    return top
}

var topBarHeight: CGFloat = statusBarHeight

var videoProgressViewHeight: (CGFloat, CGFloat) = {
    var viewHeight: CGFloat = 120
    var viewMaxY: CGFloat = 0
    if let window = UIApplication.shared.windows.first {        
        let bottomPadding = window.safeAreaInsets.bottom
        let viewMaxY = viewHeight + bottomPadding
        return (viewHeight, viewMaxY)
    }
    return (viewHeight, viewMaxY)
}()




extension Bundle {
    var displayName: String? {
        return object(forInfoDictionaryKey: "CFBundleDisplayName") as? String
    }
}

