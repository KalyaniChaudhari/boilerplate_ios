//
//  GlobalFunctions.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 22/09/21.
//

import Foundation
import UIKit

import GoogleSignIn
import FBSDKLoginKit
import AVFoundation

class GlobalFunctions : NSObject {
    
    //    add icon to hide/show the entered password
    static func addLockUnlockBtn(currentTxt: UITextField) -> UIButton {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        btn.contentEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 10)
        btn.setTitle("", for: .normal)
        btn.setImage(UIImage(named: "ic_lockPwd"), for: .normal)
        btn.setImage(UIImage(named: "ic_unlockPwd"), for: .selected)
        btn.isSelected = true
        currentTxt.rightView = btn
        btn.center = currentTxt.center
        return btn
    }
    
    
    static func showErrorSuccessDialog(isError: Bool, errorMessage: String, vc: UIViewController, completionHandler: @escaping (() -> ())) {
        let alert = UIAlertController(title: isError ? "Error" : "Success", message: errorMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            return completionHandler()
        }))
        alert.view.layoutIfNeeded()
        vc.present(alert, animated: false, completion: nil)
    }
    
   
    
    static func setRootVC(vc: UIViewController) -> UINavigationController {
        let nav = UINavigationController(rootViewController: vc)
        nav.interactivePopGestureRecognizer?.isEnabled = false
        nav.navigationBar.isTranslucent = false
        nav.navigationBar.shadowImage = UIImage()
        nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
        nav.navigationBar.tintColor = .white
        return nav
    }
    
    static func removeStoredUserData() {
        let isFirstLaunch = UserDefaults.standard.value(forKey: "isFirstLaunch")
        let userEmail = UserDefaults.standard.value(forKey: "userEmail")
        let userPassword = UserDefaults.standard.value(forKey: "userPassword")
        let deviceToken = UserDefaults.standard.value(forKey: "deviceToken")
        //        Logout from the social media through which user logged in
        self.socialMediaLogOut()
        let domain = Bundle.main.bundleIdentifier
        UserDefaults.standard.removePersistentDomain(forName: domain ?? "")
        
        UserDefaults.standard.set(deviceToken, forKey: "deviceToken")
        UserDefaults.standard.set(isFirstLaunch, forKey: "isFirstLaunch")
        UserDefaults.standard.set(userEmail, forKey: "userEmail")
        UserDefaults.standard.set(userPassword, forKey: "userPassword")
        UserDefaults.standard.synchronize()
    }
    
    static func socialMediaLogOut() {
        let socialMediaType = UserDefaults.standard.integer(forKey: "userSignInType")
        //        Google sign out if sign in type is 3
        switch socialMediaType {
        case 1:
            let loginManager = LoginManager()
            if let _ = AccessToken.current {
                loginManager.logOut()
            }
        case 3:
            GIDSignIn.sharedInstance()?.signOut()
        default:
            GlobalFunctions.printToConsole(message: "Social media login type:- \(socialMediaType)")
        }
    }
    
    static func printToConsole(message : String) {
        #if DEBUG
        print(message)
        #endif
    }
    
//    static func showErrorView(isNetworkErr: Bool, frame: CGRect) -> ErrorView {
//        let errorView = ErrorView(isNetworkError: isNetworkErr, frame: frame)
//        return errorView
//    }
//
//
//
//    //    Used for validation error label
//    static func createErrorMessageLbl(errorStr: String, lblHeight: CGFloat = 15) -> UILabel {
//        let errorLbl =  UILabel()
//        errorLbl.font = UIFont(name: Fonts.regularFont, size: 16.0)
//        errorLbl.textColor = .red
//        errorLbl.text = errorStr
//        errorLbl.autoSetDimension(.height, toSize: lblHeight)
//        return errorLbl
//    }
    
    static func navTitleView() -> UIView {
        let titleView = UIView(frame: CGRect(x: (deviceConstants.deviceWidth / 2) - 70, y: 0, width: 140, height: 44))// CGRectMake(0, 0, 150, 40))
        
        let logo = UIImage(named: "logo")
        let imageView = UIImageView(image: logo)
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.frame = titleView.bounds
        
        titleView.addSubview(imageView)
        imageView.frame.size.height = 25
        imageView.frame.origin.y = (44 - 25) / 2
        
        return titleView
    }
   
    
    
    //#MARK:- Fetch logged in user details
//    static func fetchLoggedinUserDetail() {
//        guard let userId = userDetail?._id else { return  }
//        let apiStr = ApiEndpoints.updateUserInfoApi + userId
//        NetworkRequest.sharedInstance.apiRequest(method: .get, apiStr: apiStr, param: [:]) { (userData, error) in
//            guard let userDetail = userData else {
//                GlobalFunctions.printToConsole(message: error?.localizedDescription ?? "Server error. Please try again later.")
//                return
//            }
//            do {
//                let jsonEncoder = try JSONDecoder().decode(LoginResponseModel.self, from: userDetail)
//                GlobalFunctions.printToConsole(message: "forgot password response:- \(jsonEncoder)")
//                if jsonEncoder.status == 1 {
//                    if let userData = jsonEncoder.user {
//                        //                        store user detail in userdefaults
//                        JsonConvertor.convertObjToStr(jsonObj: userData)
//                    }
//                }
//            } catch {
//                GlobalFunctions.printToConsole(message: "json error:- \(error.localizedDescription)")
//            }
//        }
//    }
//
    //    Fetch top view controller
    static func topViewController(in rootViewController: UIViewController?) -> UIViewController? {
        guard let rootViewController = rootViewController else {
            return nil
        }
        
        if let tabBarController = rootViewController as? UITabBarController {
            return topViewController(in: tabBarController.selectedViewController)
        } else if let navigationController = rootViewController as? UINavigationController {
            return topViewController(in: navigationController.visibleViewController)
        } else if let presentedViewController = rootViewController.presentedViewController {
            return topViewController(in: presentedViewController)
        }
        return rootViewController
    }
    
   
    //    #MARK:- fetch states list for country United states
    static func fetchUSStatesList() -> [String] {
        var statesArr = [String]()
        if let path = Bundle.main.path(forResource: "states_hash", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? [String:String] {
                    // do stuff
                    for (_, value) in jsonResult {
                        statesArr.append(value)
                    }
                    return statesArr
                }
            } catch {
                // handle error
                GlobalFunctions.printToConsole(message: "Display error:- \(error.localizedDescription)")
            }
        }
        return statesArr
    }
    
    static var Timestamp: String {
        return "\(NSDate().timeIntervalSince1970 * 1000)"
    }
    
    //    register device token
  
    
   
    //    Convert string to object back
    static func getPlayerMuteSettings() -> Bool? {
        return UserDefaults.standard.bool(forKey: "PlayerAudioMute")
    }
    
    static func isAppRunningForOONI360() -> Bool {
        return Bundle.main.displayName == "OONI 360"
        
    }
    
    func getMediaDuration(url: URL?) -> Float64 {
        guard let vUrl = url else { return 0 }
        let asset : AVURLAsset = AVURLAsset(url: vUrl) as AVURLAsset
        let duration : CMTime = asset.duration
        return CMTimeGetSeconds(duration)
    }
    
    func getNotificationSettings(){
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            GlobalFunctions.printToConsole(message: "Notification settings: \(settings)")
            switch settings.authorizationStatus {
            case .authorized:
                UserDefaults.standard.setValue(true, forKey: KeyConstant.KRemoteNotifciations)
                UserDefaults.standard.synchronize()
            case .denied:
                UserDefaults.standard.setValue(false, forKey: KeyConstant.KRemoteNotifciations)
                UserDefaults.standard.synchronize()
                
            case .notDetermined:
                UserDefaults.standard.setValue(false, forKey: KeyConstant.KRemoteNotifciations)
                UserDefaults.standard.synchronize()
                
            default:
                UserDefaults.standard.setValue(false, forKey: KeyConstant.KRemoteNotifciations)
                UserDefaults.standard.synchronize()
            }
        }
    }
    
    func getLocalURL(sUrl:String) -> URL? {
        
        guard let url = URL(string: sUrl) else {
            return nil
        }
        // then lets create your document folder url
         let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
      //  let documentsDirectoryURL = URL(string: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!)
        
        // lets create your destination file url
        let destinationUrl = documentsDirectoryURL.appendingPathComponent(url.lastPathComponent)
        
        return destinationUrl
    }
    
    func getLastComponetPath(sURL : String) -> String {
        guard let url = URL(string: sURL) else {
            return ""
        }
        return url.lastPathComponent
    }
}


struct JsonConvertor {
    //    Encode:- Convert object to string
    static func convertObjToStr(jsonObj: User?) {
        guard let userObj = jsonObj else { return  }
        do {
            let jsonData = try JSONEncoder().encode(userObj)
            let jsonStr = String(data: jsonData, encoding: .utf8)
            UserDefaults.standard.set(jsonStr, forKey: "userDetail")
            UserDefaults.standard.synchronize()
            GlobalFunctions.printToConsole(message: "Saved user details :\(jsonStr)")
            NotificationCenter.default.post(name: Notification.Name("ProfileUpdated"), object: nil)
        } catch {
            GlobalFunctions.printToConsole(message: "json to obj error:- \(error.localizedDescription)")
        }
    }
    
    //    Convert string to object back
    static func convertStrToObj() -> User? {
        let userDetail: User?
        if let jsonStr = UserDefaults.standard.string(forKey: "userDetail") {
            do {
                let jsonDecoder = JSONDecoder()
                let jsonObj = try jsonDecoder.decode(User.self, from: jsonStr.data(using: .utf8)!)
                userDetail = jsonObj
                GlobalFunctions.printToConsole(message: "Read user details :\(jsonObj)")

                return userDetail
            } catch {
                GlobalFunctions.printToConsole(message: "json to obj error:- \(error.localizedDescription)")
            }
        }
        return nil
    }
}




