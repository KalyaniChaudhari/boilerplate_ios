//
//  Validation.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 22/09/21.
//

import Foundation
import UIKit

class Validation {
    
    static let sharedInstance = Validation()
    
    //    E-mail validation
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //    check entered data is empty or not
    func checkLength(testStr:String) -> Bool {
        if testStr.trimmingCharacters(in: .whitespaces).isEmpty || testStr.count < 0{
            return false
        } else {
            return true
        }
    }
    
    //    password validation
    func validatePassword(testStr:String) -> Bool {
        //        (                   # Start of group
        //            (?=.*\d)        #   must contain at least one digit
        //            (?=.*[A-Z])     #   must contain at least one uppercase character
        //            (?=.*\W)        #   must contain at least one special symbol
        //               .            #     match anything with previous condition checking
        //                 {8,8}      #        length is exactly 8 characters
        //        )                   # End of group
        let passwordRegEx = "^((?=.*\\d)(?=.*[A-Z]).{8,})$"//"^.{6,}$"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        
        return passwordTest.evaluate(with: testStr)
    }
    
    func validateUserHandle(testStr:String) -> Bool {
        let handleRegix = "^(([A-Za-z0-9_.]*).{4,})$"//"^((_.*\\d)(?=.*[a-z]).{4,})$"
        let handleTest = NSPredicate(format:"SELF MATCHES %@", handleRegix)
        
        return handleTest.evaluate(with: testStr)
    }
    
    //    user name validation
    func validateUserName(testStr:String) -> Bool {
        
        let regEx = "^.{3,18}$"
        let test = NSPredicate(format: "SELF MATCHES %@", regEx)
        return test.evaluate(with: testStr)
    }
    
    //    Validate phone number
    func validatePhonenumber(value: String) -> Bool {
        let PHONE_REGEX = "^.{10,}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: value)
        return result
    }
    
//    login type:- 0: email, 1: phone, 2: handle
    func validateUserEmailHandle(str: String, completionHandler: @escaping(Int, Bool) -> ()) {
        if str.contains("@") {
                return completionHandler(0, isValidEmail(testStr: str))
        } else {
            return completionHandler(1, validateUserHandle(testStr: str))
        }
    }
    //    Validate credit card number
    func validateCardNumber(cardNumber: String) -> Bool {
        let cardPattern = "^[0-9-]{13,16}$"
        return NSPredicate(format: "SELF MATCHES %@", cardPattern).evaluate(with: cardNumber)
    }
    
    //    Validate credit card cvv
    func validateCardCVV(cvv: String) -> Bool {
        let cardPattern = "^\\d{3,4}$"
        return NSPredicate(format: "SELF MATCHES %@", cardPattern).evaluate(with: cvv)
    }
    
    //    Validate card expiry date
    func validateCardExpiryDate(expDate: String) -> Bool {
        let cardPattern = "^(0?[1-9]|1[012])/([0-9]){2}$"
        return NSPredicate(format: "SELF MATCHES %@", cardPattern).evaluate(with: expDate)
    }
}
