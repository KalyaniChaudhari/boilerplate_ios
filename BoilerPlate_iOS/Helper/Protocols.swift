//
//  Protocols.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 25/10/21.
//

import Foundation
protocol LoginSignUpDelegate: AnyObject {
    func completedProcess(isLogin: Bool, sMsg:String, isError: Bool)
}
