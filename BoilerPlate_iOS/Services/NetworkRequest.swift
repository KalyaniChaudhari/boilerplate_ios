

//
//  NetworkRequest.swift
//  BoilerPlate_iOS
//
//  Created by Kalyani on 22/09/21.
//

import Foundation
import Alamofire
import Reachability
import Toast_Swift

class NetworkRequest {
    
    static let sharedInstance = NetworkRequest()
    
    //    common api request for all method types i.e. GET, POST and  DELETE
    func apiRequest(method: HTTPMethod = .post, apiStr: String, param: [String:Any], header: HTTPHeaders? = nil,  completionHandler: @escaping((Data?, Error?) -> ())) {
        GlobalFunctions.printToConsole(message: "Api request tokenHeader:- \(header ?? nil) and parameters:- \(param)")
        
        //        check internet connection is available or not.
        if !(NetworkMangar.isConnected()) {
            DispatchQueue.main.async {
                let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
                let topVC = GlobalFunctions.topViewController(in: keyWindow?.rootViewController)!
//                GlobalFunctions.showErrorSuccessDialog(isError: true, errorMessage: UserErrorMessages.noNetworkErrMsg, vc: topVC, completionHandler: {})
                var style = ToastStyle()
                style.backgroundColor = .red
                topVC.view.makeToast(UserErrorMessages.noNetworkErrMsg,  duration: 3.0, position: .bottom, style: style)
                return
            }
        }
        
        //        Create data request for http methods including GET, POST and DELETE
        let apiRequest : DataRequest!
        if method == .post || method == .delete {
            apiRequest = AF.request(apiStr, method: method, parameters: param, encoding: JSONEncoding.default, headers: header)
        } else {
            apiRequest = AF.request(apiStr, method: method, parameters: param, headers: header)
        }
        
        apiRequest.responseJSON { (response) in
            GlobalFunctions.printToConsole(message: "Api request response for route '\(apiStr)'--------------------------------------param:- \(param)")
           GlobalFunctions.printToConsole(message: "api response:- \(response.result)")
            switch response.result {
            case .success(_):
                if let resultValue = response.value as? [String:Any] {                    
                    if let responseCode = resultValue["code"] as? Int {
                        if responseCode == 400 {
                            GlobalFunctions.removeStoredUserData()
                        } else {
                            return completionHandler(response.data, nil)
                        }
                    } else {
                        return completionHandler(response.data, nil)
                    }
                }
            case .failure(let error):
                GlobalFunctions.printToConsole(message: "Login error:- \(error.localizedDescription)")
                return completionHandler(nil, error)
            }
        }
    }
}

