//
//  AuthenticationServices.swift
//  BoilerPlate
//
//  Created by Kalyani on 21/09/21.
//
import Foundation
import Alamofire

class AuthenticationServices {
    
    /// Login api call
    /// - Parameters:
    ///   - email: user email
    ///   - password: user password
    ///   - completionHandler: use to perform some action on completion of api call
    /// - Returns: return mapped response or error to the calling function
    static func userLogin(loginObj: LoginRequestModel, completionHandler: @escaping((LoginResponseModel?, String?) -> ())) {
        let param: [String:Any] = loginObj.dictionary ?? [:]
        GlobalFunctions.printToConsole(message: "login request data:- \(ApiEndpoints.loginApi)-----\(param)")
        
        NetworkRequest.sharedInstance.apiRequest(apiStr: ApiEndpoints.loginApi, param: param) { (loginData, error) in
            guard let loginData = loginData else {
                return completionHandler(nil, error?.localizedDescription ?? "Server error. Please try again later.")
            }
            
            do {
                let jsonEncoder = try JSONDecoder().decode(LoginResponseModel.self, from: loginData)
                GlobalFunctions.printToConsole(message: "Login response:- \(jsonEncoder)")
                if let errorCode = jsonEncoder.code {
                    GlobalFunctions.printToConsole(message: "response:- \(errorCode)")
                    return completionHandler(nil, jsonEncoder.message ?? "Invalid email or password")
                } else {
                    self.storeUserDataAndAuthToken(responseData: jsonEncoder)
                    return completionHandler(jsonEncoder, nil)
                }
            } catch {
                GlobalFunctions.printToConsole(message: "json error:- \(error.localizedDescription)")
                return completionHandler(nil, error.localizedDescription)
            }
        }
    }
    
    /// sign up api call
    /// - Parameters:
    ///   - userDict: user data require to sign up
    ///   - completionHandler: use to perform some action on completion of api call
    /// - Returns: return mapped response or error to the calling function
    static func userSignUp(userDict: [String:Any], completionHandler: @escaping((LoginResponseModel?, String?) -> ())) {
        
        NetworkRequest.sharedInstance.apiRequest(apiStr: ApiEndpoints.signUpApi, param: userDict) { (signUpData, error) in
            
            guard let signUpData = signUpData else {
                return completionHandler(nil, error?.localizedDescription ?? "Server error. Please try again later.")
            }
            do {
                let jsonEncoder = try JSONDecoder().decode(LoginResponseModel.self, from: signUpData)
                GlobalFunctions.printToConsole(message: "signup response:- \(jsonEncoder)")
                if let errorCode = jsonEncoder.code {
                    GlobalFunctions.printToConsole(message: "response:- \(errorCode)")
                    return completionHandler(nil, jsonEncoder.message ?? "Invalid email or password")
                } else {
                    self.storeUserDataAndAuthToken(responseData: jsonEncoder)

                    return completionHandler(jsonEncoder, nil)
                }
            } catch {
                GlobalFunctions.printToConsole(message: "json error:- \(error.localizedDescription)")
                return completionHandler(nil, error.localizedDescription)
            }
        }
    }
    
    
    static func SocialSignUpLoginApi(loginObj: SocialLoginRequestModel, isLogin: Bool, onSuccess: @escaping((LoginResponseModel?) -> ()), onFailure: @escaping((String?) -> ())) {
        var apiStr = ApiEndpoints.socialLoginApi
        if !isLogin {
            apiStr = ApiEndpoints.socialSignupApi
        }
        let userDict = ["user":loginObj.dictionary]
        NetworkRequest.sharedInstance.apiRequest(apiStr: apiStr, param: userDict ) { (responseData, error) in
            guard let responseInfo = responseData else {
                onFailure(error?.localizedDescription)
                return
            }
            do {
                let jsonEncoder = try JSONDecoder().decode(LoginResponseModel.self, from: responseInfo)
                GlobalFunctions.printToConsole(message: "signup response:- \(jsonEncoder)")
                if let errorCode = jsonEncoder.code {
                    GlobalFunctions.printToConsole(message: "response:- \(errorCode)")
                    onFailure(jsonEncoder.message ?? "Failed to \(isLogin ? "login" : "signup")")
                } else {
                    removeAppleSignInData()
                    self.storeUserDataAndAuthToken(responseData: jsonEncoder)
                    return onSuccess(jsonEncoder)
                }
            } catch {
                GlobalFunctions.printToConsole(message: "json error:- \(error.localizedDescription)")
                return onFailure(error.localizedDescription)
                
            }
        }
    }
    
    static func removeAppleSignInData() {
        UserDefaults.standard.removeObject(forKey: "appleUserEmail")
        UserDefaults.standard.removeObject(forKey: "appleFullname")
    }
    
    //    #MARK:- Forgot passoword
    static func sendResetPasswordEmailApi(userDict: [String:Any], completionHandler: @escaping((ForgotPasswordResponseModel?, String?) -> ())) {
        NetworkRequest.sharedInstance.apiRequest(apiStr: ApiEndpoints.sendResetEmailApi, param: userDict) { (resetPasswordData, error) in
            guard let resetPasswordData = resetPasswordData else {
                return completionHandler(nil, error?.localizedDescription ?? "Server error. Please try again later.")
            }
            
            do {
                let jsonEncoder = try JSONDecoder().decode(ForgotPasswordResponseModel.self, from: resetPasswordData)
                GlobalFunctions.printToConsole(message: "forgot password response:- \(jsonEncoder)")
                return completionHandler(jsonEncoder, nil)
            } catch {
                GlobalFunctions.printToConsole(message: "json error:- \(error.localizedDescription)")
                return completionHandler(nil, error.localizedDescription)
            }
        }
    }
    
    
    static func updateUserInfoApi(userDict: [String:Any], completionHandler: @escaping((UpdateResponsetModel?, String?) -> ())) {
        NetworkRequest.sharedInstance.apiRequest(method:.put, apiStr: ApiEndpoints.updateUserInfoApi, param: userDict, header: tokenHeader) { (resetPasswordData, error) in
            guard let resetPasswordData = resetPasswordData else {
                return completionHandler(nil, error?.localizedDescription ?? "Server error. Please try again later.")
            }
            
            do {
                let jsonEncoder = try JSONDecoder().decode(UpdateResponsetModel.self, from: resetPasswordData)
                GlobalFunctions.printToConsole(message: "updateUserInfoApi password response:- \(jsonEncoder)")
                return completionHandler(jsonEncoder, nil)
            } catch {
                GlobalFunctions.printToConsole(message: "json error:- \(error.localizedDescription)")
                return completionHandler(nil, error.localizedDescription)
            }
        }
    }
    static func storeUserDataAndAuthToken(responseData: LoginResponseModel) {
        if let authToken = responseData.token {
            UserDefaults.standard.set(authToken, forKey: "headerToken")
        }
        if let userData = responseData.user {
            JsonConvertor.convertObjToStr(jsonObj: userData)
        }
    }
    static func changePasswordApi(userDict: [String:Any], completionHandler: @escaping((UpdateResponsetModel?, String?) -> ())) {
        NetworkRequest.sharedInstance.apiRequest(method:.post, apiStr: ApiEndpoints.changePasswordApi, param: userDict, header: tokenHeader) { (resetPasswordData, error) in
            guard let resetPasswordData = resetPasswordData else {
                return completionHandler(nil, error?.localizedDescription ?? "Server error. Please try again later.")
            }
            
            do {
                let jsonEncoder = try JSONDecoder().decode(UpdateResponsetModel.self, from: resetPasswordData)
                GlobalFunctions.printToConsole(message: "updateUserInfoApi password response:- \(jsonEncoder)")
                return completionHandler(jsonEncoder, nil)
            } catch {
                GlobalFunctions.printToConsole(message: "json error:- \(error.localizedDescription)")
                return completionHandler(nil, error.localizedDescription)
            }
        }
    }
    
}
